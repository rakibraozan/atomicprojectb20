<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP122863\Terms\Terms;
use App\Bitm\SEIP122863\Utility\Utility;

$terms= new Terms();
$trashedItems=$terms->trashed();

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Atomic Project</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../Resource/bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resource/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="row">
            <h2 class="navbar-text navbar-left"><a href="../../../index.php" class="active navbar-link">ALL PROJECT</a></h2>
            <h2 class="navbar-text navbar-right">Terms</h2>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
        <ol class="breadcrumb">

            <li><a href="index.php">Home</a></li>
            <li><a href="create.php">Create file</a></li>
            <li><a href="trashed.php">Trashed file</a></li>
        </ol>
    <h2 class="text-center">All Trashed Item</h2>
    <form  action="recoverMultiple.php" method="post" id="multiple" class="text-center">
        <button type="submit" class="btn btn-primary">Recover Selected Item</button>
        <button type="button" id="delete" class="btn btn-danger">Delete Selected Item</button>
        <div class="table-responsive col-md-8 col-md-offset-2 text-center ">
    <table  class="table table-striped table-bordered text-center" style="margin-top: 15px">
        <thead align="center">
        <tr>
            <th class=""><input type="checkbox" class="check" id="checkAll">  Select Item</th>
            <th class="text-center">SL</th>
            <th class="text-center">ID</th>
            <th class="text-center">Terms Title</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        <tbody>

            <?php
            $sl=0;
            foreach($trashedItems as $trashed){

            $sl++;
            ?>
            <tr class="success">
            <td><input type="checkbox" class="check" name="mark[]" value="<?php echo $trashed->id ?>"></td>
            <td><?php echo $sl?></td>
            <td><?php echo $trashed->id ?></td>
            <td><?php echo $trashed->title ?></td>
            <td><a href="recover.php?id=<?php echo $trashed->id ?>" class="btn btn-info" role="button">Recover</a>
                <a href="delete.php?id=<?php echo $trashed->id ?>" class="btn btn-danger" role="button">Delete</a>
            </td>
        </tr>
        <?php } ?>
        </tbody>

    </table>
            </div>
    </form>
</div>
</div>
<footer class="footer text-center breadcrumb" style="margin-top: 100px">
    <p>&copy; 2016 Atomicproject.</p>
</footer>

<script>
    $('#delete').on('click',function(){
        document.forms[0].action="deleteMultiple.php";
        $('#multiple').submit();
    });
    $("#checkAll").click(function () {
        $(".check").prop('checked', $(this).prop('checked'));
    });
</script>

</body>
</html>

