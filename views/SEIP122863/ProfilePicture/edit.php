<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP122863\ProfilePicture\ImageUploader;

$profilePicture= new ImageUploader();
$singleInfo= $profilePicture->prepare($_GET)->view();




?>




<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Atomic Project</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../Resource/bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resource/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="row">
            <h2 class="navbar-text navbar-left"><a href="../../../index.php" class="active navbar-link">ALL PROJECT</a></h2>
            <h2 class="navbar-text navbar-right">Profile Picture</h2>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">

                <li><a href="index.php">Home</a></li>
                <li><a href="create.php">Create file</a></li>
                <li><a href="trashed.php">Trashed file</a></li>
            </ol>

    <form role="form" action="update.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label>Name:</label>
            <input type="hidden" name="id" value="<?php echo $singleInfo->id ?>">
            <input type="text" class="form-control" id="usr" name="name" value="<?php echo $singleInfo->name?>">
        </div>
        <div class="form-group">
            <label>Upload your profile picture:</label>
            <input type="file" class="form-control" id="pwd" name="image">
            <img src="../../../Resource/Images/<?php echo $singleInfo->image_name?>" alt="image" height="100px" width="100px">
            <input type="submit" value="Update">
        </div>
    </form>
        </div>
    </div>
</div>
<footer class="footer text-center breadcrumb" style="margin-top: 100px">
    <p>&copy; 2016 Atomicproject.</p>
</footer>
</body>
</html>


