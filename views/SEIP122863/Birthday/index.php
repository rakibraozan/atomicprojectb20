<?php
session_start();
include_once ('../../../vendor/autoload.php');


use App\Bitm\SEIP122863\Birthday\Birthday;
use App\Bitm\SEIP122863\Utility\Utility;
use App\Bitm\SEIP122863\Message\Message;
$birthday = new Birthday();
$allBirthday=$birthday->index();
$totalItem=$birthday->count();
if(array_key_exists('itemPerPage',$_SESSION)) {
    if(array_key_exists('itemPerPage',$_GET)){
        $_SESSION['itemPerPage']=$_GET['itemPerPage'];
    }

}
else{
    $_SESSION['itemPerPage']=5;
}
$itemPerPage= $_SESSION['itemPerPage'];


$noOfPage=ceil($totalItem/$itemPerPage);
//Utility::d($noOfPage);
$pagination="";
if(array_key_exists('pageNo',$_GET)){
    $pageNo= $_GET['pageNo'];
}else {
    $pageNo = 1;
}
for($i=1;$i<=$noOfPage;$i++){
    $active=($i==$pageNo)?"active":"";
    $pagination.="<li class='$active'><a href='index.php?pageNo=$i'>$i</a></li>";
}

$pageStartFrom=$itemPerPage*($pageNo-1);
$allBirthday=$birthday->paginator($pageStartFrom,$itemPerPage);


?>




<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Atomic Project</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../Resource/bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resource/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="row">
            <h2 class="navbar-text navbar-left"><a href="../../../index.php" class="active navbar-link">ALL PROJECT</a></h2>
            <h2 class="navbar-text navbar-right">Birthday</h2>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
        <ol class="breadcrumb">

            <li><a href="index.php">Home</a></li>
            <li><a href="create.php">Create file</a></li>
            <li><a href="trashed.php">Trashed file</a></li>
        </ol>



    <div id="message">
        <?php
        if((array_key_exists('message',$_SESSION))&& !empty($_SESSION['message'])) {
            echo Message::message();
        }
        ?>
    </div>


    <div class="table-responsive col-md-8 col-md-offset-2 text-center ">
        <form role="form" action="index.php" class="navbar-right">
            <select  id="sel1" name="itemPerPage">
                <option>5</option>
                <option>10</option>
                <option>15</option>
                <option>20</option>
                <option>25</option>
            </select><button type="submit">View:</button>
        </form>
        <br/>
        <table class="table table-striped table-bordered" style="margin-top: 15px">
            <thead align="center">
            <tr>
                <th class="text-center">SL</th>
                <th class="text-center">ID</th>
                <th class="text-center">Name</th>
                <th class="text-center">Birth Date</th>
                <th class="text-center">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sl=0;
            foreach ($allBirthday as $birthday){
                $sl++?>
                <tr>
                    <td><?php echo $sl+$pageStartFrom?></td>
                    <td><?php echo $birthday->id ?></td>
                    <td><?php echo $birthday->name ?></td>
                    <td><?php $date = date("d-m-Y",strtotime($birthday->birthday)); echo $date; ?></td>
                    <td><a href="view.php?id=<?php echo $birthday->id ?>" class="btn btn-info" role="button">View</a>
                        <a href="edit.php?id=<?php echo $birthday->id ?>" class="btn btn-primary" role="button">Edit</a>
                        <a href="delete.php?id=<?php echo $birthday->id ?>" class="btn btn-danger" role="button">Delete</a>
                        <a href="trash.php?id=<?php echo $birthday->id ?>" class="btn btn-info" role="button">Trash</a>
                    </td>
                </tr>
            <?php }?>

            </tbody>
        </table>

        <ul class="pagination navbar-right">
            <?php if( $pageNo > 1 ): ?>
                <li><a href="index.php?pageNo=<?php $prev = $pageNo-1; echo $prev; ?>">Prev</a></li>
            <?php endif; ?>

            <?php echo $pagination; ?>

            <?php  if( $pageNo < $noOfPage ): ?>
                <li><a href="index.php?pageNo=<?php $next = $pageNo+1; echo $next;?>">Next</a></li>
            <?php endif; ?>
        </ul>
    </div>
</div>
</div>
<footer class="footer text-center breadcrumb">
    <p>&copy; 2016 Atomicproject.</p>
</footer>
<script>
    $('#message').show().delay(3000).fadeOut();
</script>

</body>
</html>

