
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Atomic Project</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../Resource/bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resource/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="row">
            <h2 class="navbar-text navbar-left"><a href="../../../index.php" class="active navbar-link">ALL PROJECT</a></h2>
            <h2 class="navbar-text navbar-right">Birthday</h2>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">

            <li><a href="index.php">Home</a></li>
            <li><a href="create.php">Create file</a></li>
            <li><a href="trashed.php">Trashed file</a></li>
        </ol>


        <h2 class="text-center">Create Date of Birth</h2><br/>


        <form class="" role="form" action="store.php" method="post">
            <div class="form-group">
                <label>Enter your name:</label>
                <input type="text" name="name" class="" id="email" placeholder="Enter your name"><br/><br/>
            </div>
            <div class="form-group">
                <label>Enter Birth Date:</label>
                <input type="date" name="birthday" class="" id="date" placeholder="mm/dd/yyyy"><br/><br/>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>

    </div>
    </div>
</div>
<footer class="footer text-center breadcrumb" style="margin-top: 100px">
    <p>&copy; 2016 Atomicproject.</p>
</footer>

</body>
</html>