<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP122863\Birthday\Birthday;

if((isset($_POST['name']))&&(!empty($_POST['birthday']))) {
    $birthday = new Birthday();
    $birthday->prepare($_POST)->store();
}
else {
    echo "Please insert some data";
}
