<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP122863\Book\Book;
use App\Bitm\SEIP122863\Utility\Utility;

$book= new Book();
$singleItem=$book->prepare($_GET)->view();
//Utility::dd($singleItem);


?>





<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Atomic Project</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../Resource/bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resource/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="row">
            <h2 class="navbar-text navbar-left"><a href="../../../index.php" class="active navbar-link">ALL PROJECT</a></h2>
            <h2 class="navbar-text navbar-right">BOOK LIST</h2>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">

                <li><a href="index.php">Home</a></li>
                <li><a href="create.php">Create file</a></li>
                <li><a href="trashed.php">Trashed file</a></li>
            </ol>

            <form role="form" action="update.php" method="post">
                <div class="form-group">
                    <label>Edit book title:</label>
                    <input type="hidden" name="id"  value="<?php echo $singleItem->id?>">
                    <input type="text" name="title" class="form-control" id="email" value="<?php echo $singleItem->title?>">
                </div>
                <button type="submit" class="btn btn-default">Update</button>
            </form>
        </div>
    </div>
</div>
<footer class="footer text-center breadcrumb" style="margin-top: 100px">
    <p>&copy; 2016 Atomicproject.</p>
</footer>
</body>
</html>