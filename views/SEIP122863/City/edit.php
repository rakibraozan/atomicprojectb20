<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP122863\City\City;

$city= new City();
$singleItem=$city->prepare($_GET)->view();

?>




<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Atomic Project</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../Resource/bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resource/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="row">
            <h2 class="navbar-text navbar-left"><a href="../../../index.php" class="active navbar-link">ALL PROJECT</a></h2>
            <h2 class="navbar-text navbar-right">CITY</h2>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">

                <li><a href="index.php">Home</a></li>
                <li><a href="create.php">Create file</a></li>
                <li><a href="trashed.php">Trashed file</a></li>
            </ol>
    <form role="form" action="update.php" method="post">
        <div class="form-group">
            <label>Edit Hobbies:</label>
            <input type="hidden" name="id"  value="<?php echo $singleItem->id?>">
            <input type="text" name="name" class="form-control" id="name" value="<?php echo $singleItem->name?>">
            <div class="form-group">
                <label for="sel1">Select City (select one):</label>
                <select name="city" class="form-control" id="sel1">
                    <option <?php if ($singleItem->city == "Dhaka") { echo "selected"; }?>>Dhaka</option>
                    <option <?php if ($singleItem->city == "Chittagong") { echo "selected"; }?>>Chittagong</option>
                    <option <?php if ($singleItem->city == "Sylhet") { echo "selected"; }?>>Sylhet</option>
                    <option <?php if ($singleItem->city == "Rajshahi") { echo "selected"; }?>>Rajshahi</option>
                    <option <?php if ($singleItem->city == "Khulna") { echo "selected"; }?>>Khulna</option>
                    <option <?php if ($singleItem->city == "Barisal") { echo "selected"; }?>>Barisal</option>
                </select>
            </div>
        </div>
        <button type="submit" class="btn btn-default">Update</button>
    </form>
        </div>
    </div>
</div>
<footer class="footer text-center breadcrumb" style="margin-top: 100px">
    <p>&copy; 2016 Atomicproject.</p>
</footer>
</body>
</html>