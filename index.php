<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Atomic Project</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="Resource/bootstrap/css/bootstrap.min.css">
    <script src="Resource/bootstrap/js/jquery.min.js"></script>
    <script src="Resource/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="row">
            <h2 class="navbar-text navbar-left"><a href="index.php" class="active navbar-link">ALL PROJECT</a></h2>
            <h2 class="navbar-text navbar-right"><a href="index.php" class="active navbar-link">BITM</a></h2>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="text-center">Atomic Project PHP B20</h2>
            <h3 class="text-center">MOHD RAKIB UDDIN</h3>
            <h4 class="text-center">SEIP - 122863</h4>
            <div class="table-responsive col-md-8 col-md-offset-2 text-center ">
                 <table class="table table-striped table-bordered" style="margin-top: 15px">
                    <thead align="center">
                    <tr>
                        <th class="text-center">SL</th>
                        <th class="text-center">Project Name</th>
                        <th class="text-center">Page Directory</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>01</td>
                            <td class="text-left">Book List</td>
                            <td><a href="views/SEIP122863/Book/index.php" class="btn btn-info" role="button">Index</a>
                                <a href="views/SEIP122863/Book/create.php" class="btn btn-default" role="button">Create</a>
                                <a href="views/SEIP122863/Book/edit.php" class="btn bg-primary" role="button">Edit</a>
                                <a href="views/SEIP122863/Book/view.php" class="btn bg-success" role="button">View</a>
                                <a href="views/SEIP122863/Book/trashed.php" class="btn btn-danger" role="button">Trashed</a>
                            </td>
                        </tr>
                        <tr>
                            <td>02</td>
                            <td class="text-left">Birthday Date</td>
                            <td><a href="views/SEIP122863/Birthday/index.php" class="btn btn-info" role="button">Index</a>
                                <a href="views/SEIP122863/Birthday/create.php" class="btn btn-default" role="button">Create</a>
                                <a href="views/SEIP122863/Birthday/edit.php" class="btn bg-primary" role="button">Edit</a>
                                <a href="views/SEIP122863/Birthday/view.php" class="btn bg-success" role="button">View</a>
                                <a href="views/SEIP122863/Birthday/trashed.php" class="btn btn-danger" role="button">Trashed</a>
                            </td>
                        </tr>
                        <tr>
                            <td>03</td>
                            <td class="text-left">Summary of Organization</td>
                            <td><a href="views/SEIP122863/Summary/index.php" class="btn btn-info" role="button">Index</a>
                                <a href="views/SEIP122863/Summary/create.php" class="btn btn-default" role="button">Create</a>
                                <a href="views/SEIP122863/Summary/edit.php" class="btn bg-primary" role="button">Edit</a>
                                <a href="views/SEIP122863/Summary/view.php" class="btn bg-success" role="button">View</a>
                                <a href="views/SEIP122863/Summary/trashed.php" class="btn btn-danger" role="button">Trashed</a>
                            </td>
                        </tr>
                        <tr>
                            <td>04</td>
                            <td class="text-left">Email</td>
                            <td><a href="views/SEIP122863/Email/index.php" class="btn btn-info" role="button">Index</a>
                                <a href="views/SEIP122863/Email/create.php" class="btn btn-default" role="button">Create</a>
                                <a href="views/SEIP122863/Email/edit.php" class="btn bg-primary" role="button">Edit</a>
                                <a href="views/SEIP122863/Email/view.php" class="btn bg-success" role="button">View</a>
                                <a href="views/SEIP122863/Email/trashed.php" class="btn btn-danger" role="button">Trashed</a>
                            </td>
                        </tr>
                        <tr>
                            <td>05</td>
                            <td class="text-left">Profile Picture</td>
                            <td><a href="views/SEIP122863/ProfilePicture/index.php" class="btn btn-info" role="button">Index</a>
                                <a href="views/SEIP122863/ProfilePicture/create.php" class="btn btn-default" role="button">Create</a>
                                <a href="views/SEIP122863/ProfilePicture/edit.php" class="btn bg-primary" role="button">Edit</a>
                                <a href="views/SEIP122863/ProfilePicture/view.php" class="btn bg-success" role="button">View</a>
                                <a href="views/SEIP122863/ProfilePicture/trashed.php" class="btn btn-danger" role="button">Trashed</a>
                            </td>
                        </tr>
                        <tr>
                            <td>06</td>
                            <td class="text-left">Gender</td>
                            <td><a href="views/SEIP122863/Gender/index.php" class="btn btn-info" role="button">Index</a>
                                <a href="views/SEIP122863/Gender/create.php" class="btn btn-default" role="button">Create</a>
                                <a href="views/SEIP122863/Gender/edit.php" class="btn bg-primary" role="button">Edit</a>
                                <a href="views/SEIP122863/Gender/view.php" class="btn bg-success" role="button">View</a>
                                <a href="views/SEIP122863/Gender/trashed.php" class="btn btn-danger" role="button">Trashed</a>
                            </td>
                        </tr>
                        <tr>
                            <td>07</td>
                            <td class="text-left">Hobby</td>
                            <td><a href="views/SEIP122863/Hobby/index.php" class="btn btn-info" role="button">Index</a>
                                <a href="views/SEIP122863/Hobby/create.php" class="btn btn-default" role="button">Create</a>
                                <a href="views/SEIP122863/Hobby/edit.php" class="btn bg-primary" role="button">Edit</a>
                                <a href="views/SEIP122863/Hobby/view.php" class="btn bg-success" role="button">View</a>
                                <a href="views/SEIP122863/Hobby/trashed.php" class="btn btn-danger" role="button">Trashed</a>
                            </td>
                        </tr>
                        <tr>
                            <td>08</td>
                            <td class="text-left">City</td>
                            <td><a href="views/SEIP122863/City/index.php" class="btn btn-info" role="button">Index</a>
                                <a href="views/SEIP122863/City/create.php" class="btn btn-default" role="button">Create</a>
                                <a href="views/SEIP122863/City/edit.php" class="btn bg-primary" role="button">Edit</a>
                                <a href="views/SEIP122863/City/view.php" class="btn bg-success" role="button">View</a>
                                <a href="views/SEIP122863/City/trashed.php" class="btn btn-danger" role="button">Trashed</a>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<footer class="footer text-center breadcrumb" style="margin-top: 100px">
    <p>&copy; 2016 Atomicproject.</p>
</footer>

</body>
</html>
