<?php
namespace App\Bitm\SEIP122863\Birthday;
use App\Bitm\SEIP122863\Utility\Utility;
use App\Bitm\SEIP122863\Message\Message;


Class Birthday{
    public $id="";
    public $name="";
    public $birthday="";
    public $conn;
    public $deleted_at;

public function prepare($data="")
{
    if (array_key_exists("name", $data)) {
        $this->name = $data['name'];
    }
    if (array_key_exists("birthday", $data)) {
        $this->birthday = $data['birthday'];
    }
    if (array_key_exists("id", $data)) {
        $this->id = $data['id'];
    }
    return $this;
}

public function __construct(){
    $this->conn= mysqli_connect("localhost","root","","atomicprojectb20") or die("Database connection establish failed");
}

    public function store(){
        $date = str_replace("/","-",$this->birthday);
        $newDate = date("Y-m-d", strtotime($date));
        $query="INSERT INTO `atomicprojectb20`.`birthday` (`name`,`birthday`) VALUES ('".$this->name."','".$newDate."')";
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data has been stored successfully.</div>");
            Utility::redirect('index.php');

        }
        else {
            Message::message("<div class=\"alert alert-info\">
  <strong>Error!</strong> Data has been stored successfully.
</div>");
            Utility::redirect('index.php');

        }

    }



    public  function index(){
        $_allBirthday=array();
        $query= "SELECT * FROM `birthday` WHERE `deleted_at` IS NULL";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $_allBirthday[]=$row;
        }
        return $_allBirthday;
    }

    public function view(){
        $query="SELECT * FROM `birthday` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_object($result);
        return $row;
    }

    public function update(){
        $date = str_replace("/","-",$this->birthday);
        $newDate = date("Y-m-d", strtotime($date));
        $query="UPDATE `atomicprojectb20`.`birthday` SET `name` = '".$this->name."',`birthday` = '".$newDate."' WHERE `birthday`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been updated successfully.
</div>");
            Utility::redirect('index.php');

        }
        else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been updated successfully.
</div>");
            Utility::redirect('index.php');

        }

    }

    public function delete()
    {
        $query = "DELETE FROM `atomicprojectb20`.`birthday` WHERE `birthday`.`id` =" . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been deleted successfully.
</div>");
            Utility::redirect('index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been deleted successfully.
</div>");
            Utility::redirect('index.php');


        }
    }
    public function trash(){
        $this->deleted_at=time();
        $query="UPDATE `atomicprojectb20`.`birthday` SET `deleted_at` = '".$this->deleted_at."' WHERE `birthday`.`id` = ".$this->id;
        $result=mysqli_query($this->conn,$query);
        if ($result) {
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been trashed successfully.
</div>");
            Utility::redirect('index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been trashed successfully.
</div>");
            Utility::redirect('index.php');


        }


    }
    public function trashed(){
        $_allBirthday=array();
        $query= "SELECT * FROM `birthday` WHERE `deleted_at` IS NOT NULL";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $_allBirthday[]=$row;
        }
        return $_allBirthday;

    }
    public function recover(){
        $query="UPDATE `atomicprojectb20`.`birthday` SET `deleted_at` =NULL WHERE `birthday`.`id` = ".$this->id;
        $result=mysqli_query($this->conn,$query);
        if ($result) {
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been recovered successfully.
</div>");
            Utility::redirect('index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been recovered successfully.
</div>");
            Utility::redirect('index.php');


        }


    }
    public function recoverMultiple($idS=array()){
        if((is_array($idS))&& count($idS)>0){
            $IDs= implode(",",$idS);
        $query="UPDATE `atomicprojectb20`.`birthday` SET `deleted_at` =NULL WHERE `birthday`.`id` IN(".$IDs.")";
        $result=mysqli_query($this->conn,$query);
        if ($result) {
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Selected Data has been recovered successfully.
</div>");
            Utility::redirect('index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Selected Data has not been recovered successfully.
</div>");
            Utility::redirect('index.php');

        }
        }


    }

    public function deleteMultiple($idS=array()){
        if((is_array($idS))&& count($idS)>0){
            $IDs= implode(",",$idS);
            $query="DELETE FROM `atomicprojectb20`.`birthday` WHERE `birthday`.`id` IN(".$IDs.")";
            $result=mysqli_query($this->conn,$query);
            if ($result) {
                Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Selected Data has been deleted successfully.
</div>");
                Utility::redirect('index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Selected Data has not been deleted successfully.
</div>");
                Utility::redirect('index.php');

            }
        }


    }
    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `birthday` WHERE `deleted_at` IS NULL";
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }

    public function paginator($pageStartFrom=0,$Limit=5){
        $_allBirthday=array();
        $query="SELECT * FROM `birthday` WHERE `deleted_at` IS NULL LIMIT ".$pageStartFrom.",".$Limit;
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $_allBirthday[]=$row;
        }
        return $_allBirthday;

    }






}