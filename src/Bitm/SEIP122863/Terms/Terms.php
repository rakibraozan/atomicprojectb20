<?php
namespace App\Bitm\SEIP122863\Terms;
use App\Bitm\SEIP122863\Utility\Utility;
use App\Bitm\SEIP122863\Message\Message;


Class Terms{
    public $id="";
    public $title="";
    public $conn;
    public $deleted_at;

public function prepare($data="")
{
    if (array_key_exists("title", $data)) {
        $this->title = $data['title'];
    }
    if (array_key_exists("id", $data)) {
        $this->id = $data['id'];
    }
    return $this;
}

public function __construct(){
    $this->conn= mysqli_connect("localhost","root","","atomicprojectb20") or die("Database connection establish failed");
}

    public function store(){
        $query="INSERT INTO `atomicprojectb20`.`terms` (`title`) VALUES ('".$this->title."')";
        //echo $query;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data has been stored successfully.</div>");
            Utility::redirect('index.php');

        }
        else {
            Message::message("<div class=\"alert alert-info\">
  <strong>Error!</strong> Data has been stored successfully.
</div>");
            Utility::redirect('index.php');

        }

    }



    public  function index(){
        $_allTerms=array();
        $query= "SELECT * FROM `terms` WHERE `deleted_at` IS NULL";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $_allTerms[]=$row;
        }
        return $_allTerms;
    }

    public function view(){
        $query="SELECT * FROM `terms` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_object($result);
        return $row;
    }

    public function update(){
        $query="UPDATE `atomicprojectb20`.`terms` SET `title` = '".$this->title."' WHERE `terms`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been updated successfully.
</div>");
            Utility::redirect('index.php');

        }
        else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been updated successfully.
</div>");
            Utility::redirect('index.php');

        }

    }

    public function delete()
    {
        $query = "DELETE FROM `atomicprojectb20`.`terms` WHERE `terms`.`id` =" . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been deleted successfully.
</div>");
            Utility::redirect('index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been deleted successfully.
</div>");
            Utility::redirect('index.php');


        }
    }
    public function trash(){
        $this->deleted_at=time();
        $query="UPDATE `atomicprojectb20`.`terms` SET `deleted_at` = '".$this->deleted_at."' WHERE `terms`.`id` = ".$this->id;
        $result=mysqli_query($this->conn,$query);
        if ($result) {
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been trashed successfully.
</div>");
            Utility::redirect('index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been trashed successfully.
</div>");
            Utility::redirect('index.php');


        }


    }
    public function trashed(){
        $_allTerms=array();
        $query= "SELECT * FROM `terms` WHERE `deleted_at` IS NOT NULL";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $_allTerms[]=$row;
        }
        return $_allTerms;

    }
    public function recover(){
        $query="UPDATE `atomicprojectb20`.`terms` SET `deleted_at` =NULL WHERE `terms`.`id` = ".$this->id;
        $result=mysqli_query($this->conn,$query);
        if ($result) {
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been recovered successfully.
</div>");
            Utility::redirect('index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been recovered successfully.
</div>");
            Utility::redirect('index.php');


        }


    }
    public function recoverMultiple($idS=array()){
        if((is_array($idS))&& count($idS)>0){
            $IDs= implode(",",$idS);
        $query="UPDATE `atomicprojectb20`.`terms` SET `deleted_at` =NULL WHERE `terms`.`id` IN(".$IDs.")";
        $result=mysqli_query($this->conn,$query);
        if ($result) {
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Selected Data has been recovered successfully.
</div>");
            Utility::redirect('index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Selected Data has not been recovered successfully.
</div>");
            Utility::redirect('index.php');

        }
        }


    }

    public function deleteMultiple($idS=array()){
        if((is_array($idS))&& count($idS)>0){
            $IDs= implode(",",$idS);
            $query="DELETE FROM `atomicprojectb20`.`terms` WHERE `terms`.`id` IN(".$IDs.")";
            $result=mysqli_query($this->conn,$query);
            if ($result) {
                Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Selected Data has been deleted successfully.
</div>");
                Utility::redirect('index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Selected Data has not been deleted successfully.
</div>");
                Utility::redirect('index.php');

            }
        }


    }
    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `terms` WHERE `deleted_at` IS NULL";
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }

    public function paginator($pageStartFrom=0,$Limit=5){
        $query="SELECT * FROM `terms` WHERE `deleted_at` IS NULL LIMIT ".$pageStartFrom.",".$Limit;
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $_allTerms[]=$row;
        }
        return $_allTerms;

    }






}